#!/bin/bash
# All signatures are retained here: https://gitlab.com/951-President-St/meeting-minutes/

# Get the directory this file is in.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Loop through all the things in the directory this script is in.
for dir in ${DIR}/*; do
    # If the current thing is a directory
    if [[ -d ${dir} ]]; then
        # then sign all the files in there.
        cd "$dir" && sha256sum -b *.pdf > files-sha256.txt;
    fi
done
